import BmiResult from './components/BmiResult'
import Lifestyle from './components/Lifestyle'
import LimitationsBmi from './components/LimitationsBmi'
import Section1 from './components/Section1'


export default function App() {
  
  return (
    <>
    <Section1/>
    <BmiResult/>
    <Lifestyle/>
    <LimitationsBmi/>
    </>
    )
}