import HealthyIcon from '../assets/images/icon-eating.svg'
import ExerciseIcon from '../assets/images/icon-exercise.svg'
import SleepIcon from '../assets/images/icon-sleep.svg'

export default function Lifestyle() {
  return (
    <div className='container  bg-blue-light md:mt-120 lg:max-w-full  md:mx-auto h-120 md:h-115 mt-14 p-8 lg:pe-52 md:p-70  lg:h-96 lg:items-center md:max-w-5xl'>
        <div className='lg:flex  lg:justify-center lg:items-center lg:gap-80 '>
              <div className='md:flex lg:flex-col lg:w-20   md:ms-8 lg:ms-0'>
            <img className='lg:mx-auto md:mt-10  md:me-8' src={HealthyIcon}/>
            <div className='lg:text-start lg:w-100'>
            <h3 className=' font-semibold text-2xl mt-10 mb-6'>Healthy eating</h3>
            <p className='text-slate-500 lg:w-84 '>Healthy eating promotes weight control, disease prevention, better digestion, immunity, mental clarity, and mood.</p>
            </div>
        </div>
        <div className='mt-8 lg:mt-0 md:mt-10 md:flex lg:flex-col lg:w-20 md:ms-8 lg:ms-0'>
            <img className='lg:mx-auto md:mt-10 md:me-8' src={ExerciseIcon}/>
            <div className='lg:text-start lg:w-100'>
            <h3 className=' font-semibold text-2xl mt-10 mb-6'>Regular exercise</h3>
            <p className='text-slate-500 lg:w-84 '>Exercise improves fitness, aids weight control, elevates mood, and reduces disease risk, fostering wellness and longevity.</p>
            </div>
        </div>
        <div className='mt-8 lg:mt-0 md:mt-10 md:flex lg:flex-col lg:w-20 md:ms-8 lg:ms-0'>
            <img className='lg:mx-auto md:mt-10 md:me-8' src={SleepIcon}/>
            <div className='lg:text-start lg:w-100'>
            <h3 className=' font-semibold text-2xl mt-10 mb-6'>Adequate sleep</h3>
            <p className='text-slate-500 lg:w-84 '>Sleep enhances mental clarity, emotional stability, and physical wellness, promoting overall restoration and rejuvenation.</p>
            </div>
        </div>

        </div>
    </div>
  )
}
