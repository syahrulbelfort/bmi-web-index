import React from 'react';
import BmiLogo from '../assets/images/logo.svg';
export default function SectionBmi() {
  return (
    <div className='container bg-custom-color h-100 md:max-w-full lg:max-w-full rounded-custom-border'>
        <div className=''>
            <img className='mx-auto pt-8' src={BmiLogo}/>
            <h1 class="text-3xl font-semibold m-6 leading-tight	text-5xl md:w-96 lg:w-112 text-center mx-auto w-100">
                Body Mass
                Index Calculator
            </h1>
            <p className='text-slate-500 text-center md:w-120 md:max-w-full  max-w-xs mx-auto'>Better understand your weight in relation to your height using our body mass index (BM) calculator. While BMI is not the sole determinant of a healthy weight, it offers a valuable starting point to evaluate your overall health and well-being.</p>
        </div>
    </div>
  ) 
}
