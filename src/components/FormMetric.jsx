import { useState } from "react"
import { calculateBMI  } from "../utils/bmiUtils";
import ResultProps from "./ResultProps";

export default function FormMetric() {
 const [height, setHeight] = useState('');
 const [weight, setWeight] = useState('');
 const [result, setResult] = useState(null);

 const handleHeightChange = (e)=>{
  const heightValue = e.target.value
  setHeight(heightValue);
  const resultBmi = calculateBMI(weight, heightValue)
  setResult(resultBmi)
 }

 const handleWeightChange = (e)=>{
  const weightValue = e.target.value
  setWeight(weightValue);
  const resultBmi = calculateBMI(weightValue, height)
  setResult(resultBmi)
 }


  return (
    <div> 
        <form className='flex md:flex-row md:gap-x-3 flex-col mt-5 mb-10'>
            <div className='relative'>
                <span className='absolute right-12 lg:right-5 md:right-16 md:bottom-4 font-bold bottom-8 text-2xl text-blue'>cm</span>
                <label className='text-slate-500	 mb-2'>Height</label>
                <input onChange={handleHeightChange} value={height} type="number" className="border-solid border-2 lg:w-60 rounded-xl mt-1	ps-4 md:w-72 md:w-80	h-24 md:h-16 font-bold		  text-2xl  border-slate-200 w-full	 placeholder-slate-400 contrast-more:border-slate-700 contrast-more:placeholder-slate-500 " />
                </div>
                <div className='relative'>
                <span className='absolute right-12 font-bold bottom-8 text-2xl md:right-16 md:bottom-4 text-blue lg:right-5'>kg</span>
                <label className=' text-slate-500	 mb-2'>Weight</label>
                <input 
                type="number" 
                onChange={handleWeightChange} 
                value={weight} 
                className="rounded-xl md:h-16 w-full lg:w-60	md:w-80 mt-1 ps-4 md:w-72	h-24 font-bold border-2	 text-2xl border-slate-200	 placeholder-slate-400 contrast-more:border-slate-400 contrast-more:placeholder-slate-500 " />
            </div>
            
    </form>
    <div className="card w-72 md:flex bg-black bg-blue-color h-72 p-6 text-white mx-auto md:w-full w-full md:rounded-e-full rounded-xl md:gap-30 items-center md:h-36 lg:gap-10">
    <ResultProps result={result}/>      
    </div>
  </div>
  )
}

