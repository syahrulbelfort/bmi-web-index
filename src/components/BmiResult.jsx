import People from '../assets/images/image-man-eating.webp'

export default function BmiResult() {
  return (
    <>
    <div className='container md:relative md:flex md:pt-20 md:gap-14 md:mt-124 mt-125 max-w-xl lg:max-w-full mx-auto lg:justify-center lg:pt-0 lg:mt-48 lg:ms-12'>
      <img className='md:w-96 md:h-96 md:-ms-105 lg:h-112 lg:w-112' src={People}/>
      <div className='p-8 lg:mt-32'>
        <h2 className='text-4xl mb-8 font-semibold md:w-110 '>What your BMI result means</h2>
        <p className='text-slate-500 max-w-96 text-base lg:w-92'>A BMI range of 18.5 to 24.9 is considered a 'healthy weight.' Maintaining a healthy weight may lower your chances of experiencing health issues later on, such as obesity and type 2 diabetes. Aim for a nutritious diet with reduced fat and sugar content, incorporating ample fruits and vegetables. Additionally, strive for regular physical activity, ideally about 30 minutes daily for five days a week.</p>
      </div>
    </div>
    </>
  )
}
