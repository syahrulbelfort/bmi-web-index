import GenderIcon from '../assets/images/icon-gender.svg';
import AgeIcon from '../assets/images/icon-age.svg';
import MuscleIcon from '../assets/images/icon-muscle.svg';
import PregnancyIcon from '../assets/images/icon-pregnancy.svg';
import RaceIcon from '../assets/images/icon-race.svg';

export default function LimitationsBmi() {
  return (
    <>
     <div className='lg:flex lg:relative lg:h-120 lg:max-w-3xl lg:mx-auto'>
        <div className='p-8 md:max-w-full md:mx-auto lg:absolute lg:-left-44'>
            <h2 className='text-3xl lg:text-5xl lg:text-start font-semibold text-center mt-14 mb-8'>Limitations of BMI</h2>
            <p className='text-center md:mx-auto md:mb-10 lg:text-start md:w-120 text-slate-500 lg:w-119'>Although BMI is often a practical indicator of healthy weight, it is not suited for every person. Specific groups should carefully consider their BMI outcomes, and in certain cases, the measurement may not be beneficial to use.</p>
        </div>
          <div className='container md:max-w-3xl    md:grid md:grid-cols-2 md:mx-auto md:-mt-10 md:gap-x-5 md:gap-y-1  md:p-5'>
            
            <div className='shadow-custom-shadow mt-5 w-82 md:mt-0   rounded-2xl bg-white p-6 h-72 lg:h-52  mx-auto lg:absolute lg:top-24 lg:-right-20'>
            <div className='flex lg:mb-4 mb-10 lg:mt-0  mt-5'>
                <img src={GenderIcon}/>
                <h3 className='font-semibold text-xl ms-3'>Gender</h3>
            </div>
            <p className='text-slate-500'>The development and body fat composition of girls and boys vary with age. Consequently, a child's age and gender are considered when evaluating their BMI.</p>
            </div>
            <div className=' w-82 shadow-custom-shadow rounded-2xl bg-white mt-4 md:mt-0 md:mb-2 mb-4 p-6 h-72 lg:h-48 mx-auto lg:absolute lg:left-52 lg:bottom-80'>
            <div className='flex  lg:mb-4 mb-10 mt-5'>
                <img src={AgeIcon}/>
                <h3 className='font-semibold text-xl ms-3'>Age</h3>
            </div>
            <p className='text-slate-500'>In aging individuals, increased body fat and muscle loss may cause BMI to underestimate body fat content.</p>
            </div>
            <div className='shadow-custom-shadow rounded-2xl lg:bottom-80 lg:-right-44 bg-white md:mb-0 mb-4 p-6 h-72 w-82 mx-auto lg:h-48 lg:mb-2 lg:absolute'>
            <div className='flex lg:mb-4  mb-10 mt-5'>
                <img src={MuscleIcon}/>
                <h3 className='font-semibold text-xl ms-3'>Muscle</h3>
            </div>
            <p className='text-slate-500'>BMI may misclassify muscular individuals as overweight or obese, as it doesn't differentiate muscle from fat.</p>
            </div>
            <div className='shadow-custom-shadow lg:absolute lg:bottom-14 lg:-left-22 lg:h-60 rounded-2xl bg-white md:mb-2 p-6 h-72 mb-4 w-82 mx-auto'>
            <div className='flex mb-10 lg:mb-4 mt-5'>
                <img src={PregnancyIcon}/>
                <h3 className='font-semibold text-xl ms-3'>Pregnancy</h3>
            </div>
            <p className='text-slate-500'>Expectant mothers experience weight gain due to their growing baby. Maintaining a healthy pre-pregnancy BMI is advisable to minimise health risks for both mother and child.</p>
            </div>
        </div>
        <div className='md:relative lg:absolute lg:bottom-6 lg:right-0'>           
               <div className='shadow-custom-shadow lg:h-60   md:-mt-22  md:mb-10 rounded-2xl bg-white mb-32 p-6 h-72 w-82 mx-auto'>
                <div className='flex mb-10 lg:mb-4 mt-5'>
                <img src={RaceIcon}/>
                <h3 className='font-semibold text-xl ms-3'>Race</h3>
                </div>
                    <p className='text-slate-500'>Certain health concerns may affect individuals of some Black and Asian origins at lower BMIs than others. To learn more, it is advised to discuss this with your GP or practice nurse.</p>
              
            </div>
        </div>
     </div>
    </>
  )
}
