import React, { useState } from 'react';
import { calculateBMIImperial } from '../utils/bmiUtils';
import ResultProps from './ResultProps';

export default function FormImperial() {
  const [heightFeet, setHeightFeet] = useState('');
  const [heightInches, setHeightInches] = useState('');
  const [weightStone, setWeightStone] = useState('');
  const [weightPounds, setWeightPounds] = useState('');
  const [result, setResult] = useState(null);

  const handleHeightChange = () => {
    const heightInInches = parseInt(heightFeet) * 12 + parseInt(heightInches);
    const resultBmi = calculateBMIImperial(weightStone, weightPounds, heightFeet, heightInInches);
    setResult(resultBmi);
  };

  const handleWeightChange = () => {
    const weightInPounds = parseInt(weightStone) * 14 + parseInt(weightPounds);
    const heightInInches = parseInt(heightFeet) * 12 + parseInt(heightInches);
    const resultBmi = calculateBMIImperial(weightStone, weightInPounds, heightFeet, heightInInches);
    setResult(resultBmi);
  };

  return (
    <div>
      <form className="mb-10 mt-5 flex flex-col">
        <label className="mb-2 text-slate-500">Height</label>
        <div className="relative flex gap-4">
          <span className="absolute bottom-8 left-28 text-2xl font-bold text-blue md:bottom-4 md:left-68 lg:left-48">ft</span>
          <input
            className="mt-1 h-24 w-full rounded-xl border-2 border-solid border-slate-200 ps-4 text-2xl font-bold placeholder-slate-400 contrast-more:border-slate-700 contrast-more:placeholder-slate-500 md:h-16 md:w-80 lg:w-60"
            value={heightFeet}
            onChange={(e) => setHeightFeet(e.target.value)}
            onBlur={handleHeightChange}
          />
          <span className="absolute bottom-8 right-6 text-2xl font-bold text-blue md:bottom-4 md:right-16 lg:right-5">in</span>
          <input
            className="mt-1 h-24 w-full rounded-xl border-2 border-solid border-slate-200 ps-4 text-2xl font-bold placeholder-slate-400 contrast-more:border-slate-400 contrast-more:placeholder-slate-500 md:h-16 md:w-72 md:w-80 lg:w-60"
            value={heightInches}
            onChange={(e) => setHeightInches(e.target.value)}
            onBlur={handleHeightChange}
          />
        </div>
        <label className="mb-2 text-slate-500">Weight</label>
        <div className="relative flex gap-4">
          <span className="absolute bottom-8 left-28 text-2xl font-bold text-blue md:bottom-4 md:left-68 lg:left-48">st</span>
          <input
            className="mt-1 h-24 w-full rounded-xl border-2 border-solid border-slate-200 ps-4 text-2xl font-bold placeholder-slate-400 contrast-more:border-slate-700 contrast-more:placeholder-slate-500 md:h-16 md:w-80 lg:w-60"
            value={weightStone}
            onChange={(e) => setWeightStone(e.target.value)}
            onBlur={handleWeightChange}
          />
          <span className="absolute bottom-8 right-6 text-2xl font-bold text-blue md:bottom-4 md:right-16 lg:right-5">lbs</span>
          <input
            className="mt-1 h-24 w-full rounded-xl border-2 border-solid border-slate-200 ps-4 text-2xl font-bold placeholder-slate-400 contrast-more:border-slate-400 contrast-more:placeholder-slate-500 md:h-16 md:w-72 md:w-80 lg:w-60"
            value={weightPounds}
            onChange={(e) => setWeightPounds(e.target.value)}
            onBlur={handleWeightChange}
          />
        </div>
      </form>
      <div className="card w-72 md:flex bg-black bg-blue-color h-72 p-6 text-white mx-auto md:w-full w-full md:rounded-e-full rounded-xl md:gap-30 items-center md:h-36 lg:gap-10">
        <ResultProps result={result} />
      </div>
    </div>
  );
}
