import { useState } from 'react';
import BmiLogo from '../assets/images/logo.svg';
import FormMetric from './FormMetric';
import FormImperial from './FormImperial';

export default function Section1() {
    const [options, setOptions] = useState('metric')


    function handleChange(e){
       setOptions( e.target.value)
    }

    

 const renderForm = () => {
  return (
    <div className={`container lg:absolute lg:bottom-20 lg:-right-100 2xl:-right-102 bg-white shadow-custom-shadow md:max-w-3xl ${options === 'imperial' ? `lg:h-116 lg:top-14 md:h-116` : `md:h-98`} md:mt-10 md:p-10  h-110 max-w-sm mx-auto mt-10 p-6 rounded-2xl  lg:w-119`}>
      <div className=''>
        <h3 className='text-center md:text-start text-2xl font-semibold'>Enter your details below</h3>
        <div className="mt-5">
          <div className='flex md:mt-10 justify-center gap-1 md:justify-start items-center'>
            <input className='me-2 w-8 h-8' type="radio" id="metric" name="fav_language" value="metric" checked={options === 'metric'} onChange={handleChange} />
            <label className='me-10 text-base font-medium' htmlFor="metric">Metric</label>
            <input className='me-2 w-8 h-8 md:ms-52 lg:ms-32' type="radio" id="imperial" name="fav_language" value="imperial" checked={options === 'imperial'} onChange={handleChange} />
            <label className='text-base font-semibold' htmlFor="imperial">Imperial</label>
          </div>
          {options === 'metric' ? <FormMetric /> : <FormImperial />}
        </div>
      </div>
    </div>
  );
}





  return (
    <>
    <div className='container bg-custom-color lg:bg-none lg:flex h-100 md:max-w-full lg:max-w-full rounded-custom-border'>
    <div className='lg:bg-custom-color lg:rounded-custom-border 2xl:w-1/2 lg:h-115  lg:w-127 lg:relative'>
      <div className='lg:absolute lg:right-100 2xl:right-24'>
            <img className='mx-auto lg:mx-0 pt-8 lg:mb-20' src={BmiLogo}/>
            <h1 class="text-3xl lg:text-start font-semibold m-6 lg:m-0 leading-tight	text-5xl md:w-96  text-center mx-auto w-100 lg:mb-10">
                Body Mass
                Index Calculator
            </h1>
            <p className='text-slate-500 text-center md:w-120 md:max-w-full lg:text-start  max-w-xs  mx-auto lg:m-0 lg:w-112 '>Better understand your weight in relation to your height using our body mass index (BM) calculator. While BMI is not the sole determinant of a healthy weight, it offers a valuable starting point to evaluate your overall health and well-being.</p>
        </div>

        {renderForm()}
      </div>
    </div>
    </>
  )
}
