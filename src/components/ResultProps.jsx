import { getBMICategory } from "../utils/bmiUtils";

export default function ResultProps(props) {
    const { result } = props;

    const category = getBMICategory(result)
    
  
    return (
      <>
        {result ? (
          <>
            <p className='font-semibold mt-5 md:mt-0'>
              Your BMI is ... <br />
              <span className='font-bold mt-5 md:mt-0 text-5xl'>{result}</span>
            </p>
        { category ==="healthy" ? (
             <p className='mt-5 lg:mt-0 md:flex-initial w-80'>
             Your BMI suggests you’re a healthy weight. A BMI range of <span className='font-bold'>18.5 to 24.9</span>{` `}
            is considered a healthy weight.
           </p>
        ) : (
            <p className='mt-5 lg:mt-0 md:flex-initial w-70 lg:mt-0 lg:w-80'>
                Your BMI suggests you’re not at a healthy weight. It is recommended to consult with a healthcare professional. 
          </p>
        ) }
         
          </>
        ) : (
          <p className='lg:text-2xl lg:text-start md:text-2xl md:text-start text-2xl text-center font-semibold mt-14 md:mt-0 lg:mt-0'>
            Welcome! <br/>
            <span className='lg:text-sm text-lg font-normal'>
              Enter your height and weight and you’ll see your BMI result here
            </span>
          </p>
        )}
      </>
    );
  }
  