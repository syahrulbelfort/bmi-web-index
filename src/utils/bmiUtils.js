export const calculateBMI = (weight, height) => {
    const weightInKg = parseFloat(weight);
    const heightInM = parseFloat(height) / 100;
  
    if (weightInKg && heightInM) {
      const bmiValue = weightInKg / (heightInM * heightInM);
      return bmiValue.toFixed(2);
    } else {
      return null;
    }
  };

  export const calculateBMIImperial = (weightStone, weightPounds, heightFeet, heightInches) => {
    const weightInLbs = (parseInt(weightStone) * 14) + parseInt(weightPounds);
    const heightInInches = (parseInt(heightFeet) * 12) + parseInt(heightInches);
  
    if (weightInLbs && heightInInches) {
      const bmiValue = (weightInLbs / (heightInInches * heightInInches)) * 703;
      return bmiValue.toFixed(2);
    } else {
      return null;
    }
  };
  
  
  
  // You can also add additional utility functions related to BMI calculations if needed
  // For example, a function to categorize BMI into different ranges.
  export const getBMICategory = (bmi) => {
    // Define your own BMI categories and ranges here
    if (bmi < 18.5) {
      return 'Not healthy';
    } else if (bmi < 25) {
      return 'healthy';
    } else if (bmi < 30) {
      return 'Not healthy';
    } else {
      return 'Not healthy';
    }
  };
  