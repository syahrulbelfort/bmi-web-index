/* @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      width: {
        "18": '208px',
        "100": '350px',
        "110": '368px',
        "112": '480px',
        "90": "390px",
        "92": "450px",
        "82": "360px",
        "115" : "504px",
        "119" : "564px",
        "120": "700px",
        "130": "600px",
        "125": "55%",
        "126": "480px",
        "127": "65%",
        "custom": "60%",
      },
      height: {
        "100": '540px',
        "120": "700px",
        "112": '480px',
        "115": '650px',
        "116": '600px',
        "98": '497px',
        "102": '450px',
        '110': '749px',
        "120" : "850px"
      },
      backgroundColor:{
        'people-bg' :'#E7F5FE'
      },
      backgroundImage: {
        "custom-color": "linear-gradient(290.1deg, #D6E6FE 0%, rgba(214, 252, 254, 0) 100%)",
        "blue-color": "linear-gradient(90deg, #345FF7 0%, #587DFF 100%);",
        "blue-light" : "linear-gradient(290.1deg, #D6E6FE 0%, rgba(214, 252, 254, 0) 100%);"
      },
      borderRadius: {
        "custom-border": "0px 0px 35px 35px"
      },
      textColor:{
        "blue" : '#345FF6'
      },
      padding:{
        "130" : "330px",
      },
      inset:{
        "100" : "300px",
        "102" : "480px",
        "68" : "270px",
      },
      margin: {
        "-100": "-100px",
        "-110": "-250px",
        "-105": "-180px",
        "120" :"190px",
        "115" :"30px",
        "113" :"160px",
        "124" :"400px",
        "125" :"750px",
        "123" :"660px"
      },
      gap : {
        "30" : "220px"
      }
    },
    boxShadow: {
      "custom-shadow": '16px 32px 56px rgba(143, 174, 207, 0.25)',
    },
  },
  plugins: [],
};
